# import the necessary packages
from tensorflow.keras.applications.resnet import preprocess_input
from tensorflow.keras.preprocessing.image import img_to_array
from tensorflow.keras.applications import imagenet_utils
from imutils.object_detection import non_max_suppression
from pyimagesearch.detection_helpers import sliding_window
from pyimagesearch.detection_helpers import image_pyramid
import numpy as np
import argparse
import imutils
import time
import cv2
import tensorflow as tf
from tensorflow.keras.layers import Dropout, Flatten, Dense
import os
numberOfClass = 10
from tensorflow.keras.applications import MobileNetV3Small

class staticVariable:
	Confidencescore =0

staticVar = staticVariable()


class_names = ["airplane", "automobile", "bird", "cat", "deer", "dog", "frog", "horse", "ship", "truck"]
config = tf.compat.v1.ConfigProto()
config.gpu_options.allow_growth = True
Modeselection = 0; #0:test 1: visualmode
sess = tf.compat.v1.Session(config=config)


model = tf.keras.applications.MobileNetV3Small(
    input_shape=None, alpha=1.0, minimalistic=False, include_top=True,
    weights='imagenet', input_tensor=None, classes=1000, pooling=None,
   dropout_rate=0.2, classifier_activation='softmax'
)
last = model.layers[-1].output
x = Flatten()(last)
#x = Dense(1000, activation='relu', name='fc1')(x)
#x = Dropout(0.2)(x)
x = Dense(10, activation='relu', name='predictions')(x)
model = tf.keras.Model(model.input, x)
checkpoint_path = "training_1/cp.ckpt"
model.load_weights(checkpoint_path)

model.summary()
# construct the argument parse and parse the arguments
ap = argparse.ArgumentParser()
#ap.add_argument("-i", "--image", required=True,
				#help="images/lawn_mower.jpg")
#ap.add_argument("-s", "--size", type=str, default="(200, 200)",
					#help="ROI size (in pixels)")
ap.add_argument("-c", "--min-conf", type=float, default=0.1,
	help="minimum probability to filter weak detections")
ap.add_argument("-v", "--visualize", type=int, default=-1,
	help="whether or not to show extra visualizations for debugging")
args = vars(ap.parse_args())

# initialize variables used for the object detection procedure
WIDTH = 400
PYR_SCALE = 1.5
WIN_STEP = 16
ROI_SIZE = eval("(200, 200)")
INPUT_SIZE = (112, 112)


# load our network weights from disk
print("[INFO] loading network...")
#model = ResNet50(weights="imagenet", include_top=True)
# load the input image from disk, resize it such that it has the
# has the supplied width, and then grab its dimensions
x = []
def load_images_from_folder(folder):
    images = []
    for filename in os.listdir(folder):
        img = cv2.imread(os.path.join(folder,filename))
        if img is not None:
            images.append(img)
    return images

directory_images = load_images_from_folder("images/")

for singleimage in directory_images:
    orig = singleimage
    orig = imutils.resize(orig, width=WIDTH)
    (H, W) = orig.shape[:2]

    # initialize the image pyramid
    pyramid = image_pyramid(orig, scale=PYR_SCALE, minSize=ROI_SIZE)
    # initialize two lists, one to hold the ROIs generated from the image
    # pyramid and sliding window, and another list used to store the
    # (x, y)-coordinates of where the ROI was in the original image
    rois = []
    locs = []
    # time how long it takes to loop over the image pyramid layers and
    # sliding window locations
    start = time.time()

 # loop over the image pyramid
    for image in pyramid:
	# determine the scale factor between the *original* image
	# dimensions and the *current* layer of the pyramid
	    scale = W / float(image.shape[1])
	# for each layer of the image pyramid, loop over the sliding
	# window locations
	    for (x, y, roiOrig) in sliding_window(image, WIN_STEP, ROI_SIZE):
		# scale the (x, y)-coordinates of the ROI with respect to the
		# *original* image dimensions
		    x = int(x * scale)
		    y = int(y * scale)
		    w = int(ROI_SIZE[0] * scale)
		    h = int(ROI_SIZE[1] * scale)
		# take the ROI and preprocess it so we can later classify
		# the region using Keras/TensorFlow
		    roi = cv2.resize(roiOrig, INPUT_SIZE)
		    roi = img_to_array(roi)
		    roi = preprocess_input(roi)
		    # update our list of ROIs and associated coordinates
		    rois.append(roi)
		    locs.append((x, y, x + w, y + h))

		    # check to see if we are visualizing each of the sliding
		    # windows in the image pyramid
		    if args["visualize"] > 0:
			    # clone the original image and then draw a bounding box
			    # surrounding the current region
			    clone = orig.copy()
			    cv2.rectangle(clone, (x, y), (x + w, y + h),
				(0, 255, 0), 2)
			    # show the visualization and current ROI
			    if (Modeselection == 1):
			        cv2.imshow("Visualization", clone)
			        cv2.imshow("ROI", roiOrig)
			        cv2.waitKey(0)
 # show how long it took to loop over the image pyramid layers and
 # sliding window locations
    end = time.time()
    if (Modeselection == 1):
        print("[INFO] looping over pyramid/windows took {:.5f} seconds".format(
	end - start))
    # convert the ROIs to a NumPy array
    rois = np.array(rois, dtype="float32")
    # classify each of the proposal ROIs using ResNet and then show how
    # long the classifications took
    if (Modeselection == 1):
        print("[INFO] classifying ROIs...")
    start = time.time()
    preds = model.predict(rois)
    end = time.time()
    if (Modeselection == 1):
        print("[INFO] classifying ROIs took {:.5f} seconds".format(
	end - start))
 # decode the predictions and initialize a dictionary which maps class
 # labels (keys) to any ROIs associated with that label (values)
 #preds = decode_predictions(preds, top=1)
    if (Modeselection == 1):
        print("MAX ELEAMAN ındex ::: "+ str(preds.argmax()) + "value:"+ str(preds.max()))
    pred_labels = np.argmax (preds, axis = -1)
    pred_label_names = [class_names [x] for x in pred_labels]
        # loop over the predictions
    for (i, p) in enumerate(preds):
	# grab the prediction information for the current ROI
	#(imagenetID, label, prob) = p[0]
	    imagenetID = pred_labels[:preds.argmax()]
	    label = pred_label_names[:preds.argmax()]
	    prob = preds.max()

	# filter out weak detections by ensuring the predicted probability
	# is greater than the minimum probability
	    if prob >= args["min_conf"]:
		# grab the bounding box associated with the prediction and
		# convert the coordinates
		    box = locs[int(preds.argmax()/numberOfClass)]
		# grab the list of predictions for the label and add the
		# bounding box and probability to the list
		    L = label
		    L.append((box, prob))
		    labels = L

 # loop over the labels for each of detected objects in the image
 #for label in labels.keys():
 # clone the original image so that we can draw on it
    if (Modeselection == 1):
        print("[INFO] showing results for '{}'".format(label))
        print("[INFO] showing results for '{}'".format(prob))
    clone = orig.copy()
 # loop over all bounding boxes for the current label
 #for (box, prob) in labels[label]:
 # draw the bounding box on the image
    (startX, startY, endX, endY) = box
    cv2.rectangle(clone, (startX, startY), (endX, endY),
			(0, 255, 0), 2)
 # show the results *before* applying non-maxima suppression, then
 # clone the image again so we can display the results *after*
 # applying non-maxima suppression
 #cv2.imshow("Before", clone)
    clone = orig.copy()

 # extract the bounding boxes and associated prediction
 # probabilities, then apply non-maxima suppression
    boxes = box
    proba = prob
    boxes = non_max_suppression(boxes, proba)
 # loop over all bounding boxes that were kept after applying
 # non-maxima suppression
 #for (startX, startY, endX, endY) in boxes:
 # draw the bounding box and label on the image
    staticVar.Confidencescore = staticVar.Confidencescore + prob;
    if (Modeselection == 1):
        cv2.rectangle(clone, (startX, startY), (endX, endY),
			(0, 0, 0), 2)
        y = startY - 10 if startY - 10 > 10 else startY + 10
        mergedString = str("class:  ") + str(label[int(preds.argmax()/numberOfClass)]) + str("  Confidence score :  ") + str(prob)
        cv2.putText(clone, mergedString, (startX, y),
			cv2.FONT_HERSHEY_SIMPLEX, 0.45, (0, 0, 0), 2)
	# show the output after apply non-maxima suppression
        cv2.imshow("After", clone)
        cv2.waitKey(0)

print(str("Total Confidence score :  ") + str(staticVar.Confidencescore/(len(directory_images))))
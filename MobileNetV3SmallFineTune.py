import tensorflow as tf
from tensorflow.keras import callbacks
from tensorflow.keras import optimizers
#from tensorflow.python.keras.engine import Model
import os
import torch.nn as nn
from tensorflow.keras.layers import Dropout, Flatten, Dense
from tensorflow.keras.optimizers import Adam, SGD,Adamax, Adadelta, Adagrad, Nadam, Ftrl
from tensorflow.keras.datasets import cifar10
from tensorflow.python.keras.preprocessing.image import ImageDataGenerator
import matplotlib.pyplot as plt

from tensorflow.keras import regularizers
from tensorflow.keras.utils import to_categorical
import numpy as np
from tensorflow.keras.applications import MobileNetV3Small, EfficientNetB7
import argparse
physical_devices = tf.config.experimental.list_physical_devices('GPU')
assert len(physical_devices) > 0, "Not enough GPU hardware devices available"
config = tf.config.experimental.set_memory_growth(physical_devices[0], True)

input_shape = (32, 32, 3)
(X_train, y_train), (X_test, y_test) = cifar10.load_data()

Y_train = tf.keras.utils.to_categorical(y_train)
Y_test = tf.keras.utils.to_categorical(y_test)
# resize train set
X_train_resized = []
for img in X_train:
    X_train_resized.append(np.resize(img, input_shape) / 255)

X_train_resized = np.array(X_train_resized)
print(X_train_resized.shape)
# resize test set
X_test_resized = []
for img in X_test:
    X_test_resized.append(np.resize(img, input_shape) / 255)

X_test_resized = np.array(X_test_resized)
print(X_test_resized.shape)

BaseModel = MobileNetV3Small(
    input_shape=None, alpha= 1.0, minimalistic=False, include_top=True,
    weights='imagenet', input_tensor=None, classes=1000, pooling=None,dropout_rate=0.2, classifier_activation='softmax'
)
#model.summary()

# We freeze every layer in our base model so that they do not train, we want that our feature extractor stays as before --> transfer learning
for layer in BaseModel.layers:
  layer.trainable = True
  #BaseModel.layers[0].trainable = True MAKE SPECIFIC LAYER TRAINABLE
  print('Layer ' + layer.name + ' frozen.')
#print('HEEYEYYYYY '+  BaseModel.layers[0].name) CAN REACH WANTED LAYER WITH IT
# We take the last layer of our the model and add it to our classifier
last = BaseModel.layers[-1].output
x = Flatten()(last)
#x = Dense(10, activation='softmax', name='fc1')(x)
x = Dense(10, activation='relu', name='predictions')(x)
model = tf.keras.Model(BaseModel.input, x)
# We compile the model
model.compile(optimizer=Adam(lr=0.001), loss='mean_squared_logarithmic_error', metrics=['accuracy'])

#model.summary()


#Save model
checkpoint_path = "training_1/cp.ckpt"
checkpoint_dir = os.path.dirname(checkpoint_path)

# Create a callback that saves the model's weights
cp_callback = tf.keras.callbacks.ModelCheckpoint(filepath=checkpoint_path,
                                                 save_weights_only=True,
                                                 verbose=1)


# We start the training
epochs = 100
batch_size = 32
for val in range(1, epochs):
 # We train it
 #model.compile(optimizer=Adamax(lr=0.001*val), loss='mean_squared_logarithmic_error', metrics=['accuracy'])
 history = model.fit(X_train_resized, Y_train,
          batch_size=batch_size,
          validation_data=(X_test_resized, Y_test),
          epochs= epochs,
          shuffle=True,
          callbacks=[cp_callback])
 pattern = 'Tell me  my learning rate: %f'
 print(history.history.keys())
 #  "Accuracy"
 plt.plot(history.history['accuracy'])
 plt.plot(history.history['val_accuracy'])
 plt.title('model accuracy')
 plt.ylabel('accuracy')
 plt.xlabel('epoch')
 plt.legend(['train', 'validation'], loc='upper left')
 plt.show()
 # "Loss"
 plt.plot(history.history['loss'])
 plt.plot(history.history['val_loss'])
 plt.title('model loss')
 plt.ylabel('loss')
 plt.xlabel('epoch')
 plt.legend(['train', 'validation'], loc='upper left')
 plt.show()

 scores = model.evaluate(X_test_resized, Y_test, verbose=1)
 print('Test loss:', scores[0])
 print('Test accuracy:', scores[1])
# We evaluate the accuracy and the loss in the test set

